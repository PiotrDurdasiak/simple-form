import React from 'react';
import logo from './logo.svg';
import './App.css';
import Formular from "./components/registration/registration";

function App() {
    return (
        <div className="App">
            <Formular />
        </div>
    );
}

export default App;
