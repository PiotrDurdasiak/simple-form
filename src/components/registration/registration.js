import React, { useState, Component } from 'react';
import ReactDOM from "react-dom";

const WrongInput = ({ text}) => {
    return (
        <div>
            <span>{text}</span>
        </div>
    )
}

const checkForm = form => {
    if (form.name.length < 2){
        return "Name is not valid"
    }

    if (form.surname.length < 2){
        return "Surname is not valid"
    }

    if(! form.email.includes('@')) {
        return "Email address is not valid"
    }
    if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/i.test(form.password)) {
        return "Password should contain at least 8 characters, one uppercase, one lowercase and one numeric digit"
    }

    if (! form.address){
        return "Address is required"
    }

    return null
}


export default function Formular () {
    const [error, setError] = useState(null);
    const [form, setForm] = useState({
        name: "",
        surname: "",
        email: "",
        password: "",
        address: ""
    });

    const updateInput = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const clickSubmit = async (e) => {
        e.preventDefault()
        const errorMessage = checkForm(form)
        if (errorMessage) {
            setError (errorMessage)
        }
        console.log('form sumited', form)
    }

    return(
        <div className="main">
            {error && <WrongInput style={{color: "red"}} text={error} />}
            <form action="" onSubmit={clickSubmit}>
                <legend>Registration</legend>
                <label htmlFor="name">Name</label>
                <input type="text" name="name" onChange={updateInput}/>
                <label htmlFor="surname">Surname</label>
                <input type="text" name="surname" onChange={updateInput}/>
                <label htmlFor="email">Email address</label>
                <input type="text" name="email" onChange={updateInput}/>
                <label htmlFor="password">Password</label>
                <input type="password" name="password" onChange={updateInput}/>
                <label htmlFor="address">Address</label>
                <input type="text" name="address" onChange={updateInput}/>
                <input type="submit" value="Register"></input>
            </form>
        </div>
)
};